# ModOrganizer2 Support Plugin for Horizon Zero Down

With this extension you can create an instance for the game in MO2 and use MO2 to manage the plugins you dowloaded from Nexusmods. 

Usage: Copy the game_horizonzerodown.py script from the plugins\basic_games\games\ folder to the MO2's similar folder. (For example if you installed MO2 to the path d:\Games\MO2\, you should put the script file to d:\Games\MO2\plugins\basic_games\games\ folder). After that open/restart MO2 than you can find the new supported game in the Instance Manager.

Important:
When you install a plugin from nexusmod be careful to place all files to the default folder (Packed_DX12)!