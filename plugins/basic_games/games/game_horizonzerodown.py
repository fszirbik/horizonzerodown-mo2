from ..basic_game import BasicGame


class XP11Game(BasicGame):
    Name = "Horizon Zero Down Support Plugin"
    Author = "SziFe"
    Version = "0.0.1"

    GameName = "Horizon Zero Down"
    GameShortName = "HorizonZeroDown"    
    GameNexusName = "horizonzerodawn"
    GameNexusId = 3381
    GameSteamId = 1151640
    GameBinary = "HorizonZeroDawn.exe"
    GameDataPath = "%GAME_PATH%/Packed_DX12"
    GameSavesDirectory = "%DOCUMENTS%\Horizon Zero Dawn\Saved Game"
